import Vue from 'vue'
import { config } from '@vue/test-utils'

Vue.config.silent = true

config.stubs.nuxt = { template: '<div />' }