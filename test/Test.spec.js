import {
  given, when, then,
  Given, When, Then,
  Rules,
  unit,
} from 'jest-bdd'

import { shallowMount, createLocalVue } from '@vue/test-utils'

import { CachingClient } from '@openteamsinc/caching-client'

import ScrudComponent from '../src/lib-components/components/ScrudComponent'

const localVue = createLocalVue()

localVue.use(CachingClient)

const rules = Rules()

Given(rules.rules, () => {})

When(rules.mount_component, () => {
  scope.component = shallowMount(ScrudComponent, {})
})

Then(rules.check_successful_mount, () => {
  expect(component.vm).toBeTruthy()
})

Then(rules.check_successful_render, () => {
  expect(component.html()).toMatchSnapshot()
})

rules.useWith(() => unit(
  given.rules(
    when.mount_component(
      then.check_successful_mount,
      then.check_successful_render
    )
  )
))