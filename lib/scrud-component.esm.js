import { SSRFormGen } from '@openteamsinc/ssr-form-gen';

class UISchema {
  constructor() {
    this.uiSchema = {};
  }

  createNode(as, props = {
    required: false,
    children: []
  }) {
    this.uiSchema[this.generateNextKey()] = new UISchemaNode(as, props);
  }

  addNode(node) {
    this.uiSchema[this.generateNextKey()] = node;
  }

  getUISchema() {
    let schema = {};
    Object.keys(this.uiSchema).forEach((node, index) => {
      schema[index] = this.uiSchema[node].node;
    });
    return schema;
  }

  getNodes() {
    return this.uiSchema;
  }

  generateNextKey() {
    return Object.keys(this.uiSchema).length;
  }

}

class UISchemaNode {
  constructor(as, props = {
    required: false,
    children: []
  }, label = null) {
    this.node = {
      as: as,
      label: label,
      props: props
    };
  }

  setChildren(children) {
    this.node.props.children = children;
  }

  setFieldOptions(props) {
    this.node.props = props;
  }

  getSchema() {
    let schema = {
      as: this.node.as,
      props: this.node.props
    };
    this.node.children.forEach(child => {
      schema.props.children.push(child);
    });
    return schema;
  }

  getNode() {
    return this.node;
  }

  getChildren() {
    return this.node.props.children;
  }

  getFieldOptions() {
    return this.node.props;
  }

  getComponent() {
    return this.node.as;
  }

}

//
var script = {
  props: {
    scrudResourceURL: {
      type: String
    },
    configMapping: {
      type: Object
    },
    uiType: {
      type: String
    },
    nodeRdfType: {
      type: String,
      required: false
    },
    data: {
      type: [Object, Array, String, Number],
      required: false
    },
    componentData: {
      type: Object,
      required: false
    },
    optionsBody: {
      type: Object,
      required: false
    },
    postBodyContext: {
      type: Object,
      required: false
    },
    getBodyResponse: {
      type: Object,
      required: false
    },
    getDataResponse: {
      type: Object,
      required: false
    },
    putBodyResponse: {
      type: Object,
      required: false
    },
    putDataResponse: {
      type: Object,
      required: false
    }
  },
  components: {
    'SSRFormGen': SSRFormGen
  },

  data() {
    return {
      uiSchemaObject: new UISchema(),
      uiSchema: {},
      components: [],
      schemaURL: null,
      contextURL: null,
      body: {},
      postBody: {},
      getBody: {},
      putBody: {},
      getData: {},
      putData: {}
    };
  },

  async fetch() {
    // Fetch options to get schema and context URLs then call according method to generate UI
    this.body = this.optionsBody ? this.optionsBody : await this.$cachingClient.options(this.convertToHttps(this.scrudResourceURL), {
      json: true
    });
    const requestType = this.body[this.uiType];

    switch (this.uiType) {
      case 'post':
        this.schemaURL = this.getPostSchemaURL(requestType);
        this.contextURL = this.getPostContextURL(requestType);
        this.postBody = this.postBodyContext ? this.postBodyContext : await this.$cachingClient.get(this.convertToHttps(this.contextURL), {
          json: true
        });
        return this.generateUIPost();

      case 'get':
        this.schemaURL = this.getSchemaURL(requestType);
        this.contextURL = this.getContextURL(requestType);
        this.getBody = this.getBodyResponse ? this.getBodyResponse : await this.$cachingClient.get(this.convertToHttps(this.contextURL), {
          json: true
        });
        this.getData = this.getDataResponse ? this.getDataResponse : await this.$cachingClient.get(this.convertToHttps(this.scrudResourceURL), {
          json: true
        });
        return this.generateUIGet();

      case 'put':
        this.schemaURL = this.getPostSchemaURL(requestType);
        this.contextURL = this.getPostContextURL(requestType);
        this.putBody = this.putBodyResponse ? this.putBodyResponse : await this.$cachingClient.get(this.convertToHttps(this.contextURL), {
          json: true
        });
        this.putData = this.putDataResponse ? this.putDataResponse : await this.$cachingClient.get(this.convertToHttps(this.scrudResourceURL), {
          json: true
        });
        return this.generateUIPut();
    }
  },

  methods: {
    getPostSchemaURL(data) {
      return data.requestBody.content['application/json'].schema;
    },

    getPostContextURL(data) {
      return data.requestBody.content['application/json'].context;
    },

    getSchemaURL(data) {
      return data.responses['200'].content['application/json'].schema;
    },

    getContextURL(data) {
      return data.responses['200'].content['application/json'].context;
    },

    convertToHttps(url) {
      if (url) {
        const to = new URL(url);
        to.protocol = 'https';
        return to.toString();
      }
    },

    generateUIPost() {
      if (this.nodeRdfType) {
        const subType = this.nodeRdfType;
        const children = [];
        const fieldOptions = this.data;
        const superTypeNode = new UISchemaNode(subType, fieldOptions.props, fieldOptions.label);
        superTypeNode.setChildren(children);
        this.uiSchema = superTypeNode;
      } else {
        // Key for supertype can possibly be @type or @id
        let typeDefine = this.postBody['@type'] ? this.postBody['@type'] : this.postBody['@id'];
        let superType = this.getComponentByRdfType(typeDefine);
        let superFieldOptions = {
          context: this.postBody,
          url: this.scrudResourceURL,
          requestType: this.uiType
        };
        let superTypeNode = new UISchemaNode(superType, superFieldOptions);
        let children = [];
        let afterType = false;
        let previousType;
        Object.keys(this.postBody).forEach(key => {
          // If the keys are after the super type definition
          if (afterType) {
            // Key for rdf type after the supertype can be notated by @id or be the value
            let subTypeDefine = this.postBody[key]['@id'] ? this.postBody[key]['@id'] : this.postBody[key];
            let component = this.getComponentByRdfType(subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine);
            this.getLabelFromKey(key);
            const props = {
              required: false
            };
            const node = {
              nodeRdfType: component,
              scrudResourceURL: this.scrudResourceURL,
              configMapping: this.configMapping,
              uiType: this.uiType,
              data: {
                label: null,
                as: component,
                props: props
              },
              optionsBody: this.body,
              postBodyContext: this.postBody
            };
            previousType = subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine;

            if (component !== 'String') {
              children.push(node);
            }
          } else {
            afterType = key === '@id' || key === '@type';
          }
        });
        superTypeNode.setChildren(children);
        this.uiSchemaObject.addNode(superTypeNode);
        this.uiSchema = this.uiSchemaObject.getUISchema();
      }
    },

    generateUIGet() {
      // Check if component is a leaf node child
      if (this.nodeRdfType) {
        const subType = this.nodeRdfType;
        const fieldOptions = { ...this.componentData,
          required: false,
          data: this.data
        };
        const superTypeNode = new UISchemaNode(subType, fieldOptions);
        this.uiSchema = superTypeNode;
      } else {
        if (this.getBody.content && this.getBody.content['@container']) {
          const superType = this.getComponentByRdfType(this.getBody.content['@container']);
          const superTypeNode = new UISchemaNode(superType);
          const subType = this.getComponentByRdfType(this.getBody.content['openteams:envelopeContents']);
          const href = this.getData.content[0] ? this.getData.content[0].href : false;

          if (href) {
            const children = [];
            this.getData.content.forEach(dataItem => {
              const cardFieldOptions = {
                nodeRdfType: subType,
                data: dataItem,
                scrudResourceURL: this.scrudResourceURL,
                configMapping: this.configMapping,
                uiType: this.uiType,
                optionsBody: this.body,
                getBodyResponse: this.getBody,
                getDataResponse: this.getData
              };
              children.push(cardFieldOptions);
            });
            superTypeNode.setChildren(children);
          }

          this.uiSchemaObject.addNode(superTypeNode);
          this.uiSchema = this.uiSchemaObject.getUISchema();
        } else {
          const superType = this.getComponentByRdfType(this.getBody['@id']);
          const superTypeFieldOptions = {
            data: this.getData
          };
          const superTypeNode = new UISchemaNode(superType, superTypeFieldOptions);
          const children = [];
          let afterType = false;
          let previousType;
          Object.keys(this.getBody).forEach(key => {
            if (afterType) {
              const typeDefine = this.getBody[key]['@id'] ? this.getBody[key]['@id'] : this.getBody[key];
              const component = this.getComponentByRdfType(typeDefine === 'schema:sameAs' ? previousType : typeDefine);
              const label = this.getLabelFromKey(key);
              const fieldOptions = {
                data: this.getData.details.content[key],
                componentData: {
                  label: label
                },
                nodeRdfType: component,
                scrudResourceURL: this.scrudResourceURL,
                configMapping: this.configMapping,
                uiType: this.uiType,
                optionsBody: this.body,
                getBodyResponse: this.getBody,
                getDataResponse: this.getData
              };
              children.push(fieldOptions);
              previousType = typeDefine === 'schema:sameAs' ? previousType : typeDefine;
            } else {
              afterType = key === '@id';
            }
          });
          superTypeNode.setChildren(children);
          this.uiSchemaObject.addNode(superTypeNode);
          this.uiSchema = this.uiSchemaObject.getUISchema();
        }
      }
    },

    generateUIPut() {
      if (this.nodeRdfType) {
        const subType = this.nodeRdfType;
        const children = [];
        const fieldOptions = this.data;
        const superTypeNode = new UISchemaNode(subType, fieldOptions.props, fieldOptions.label);
        superTypeNode.setChildren(children);
        this.uiSchema = superTypeNode;
      } else {
        // Key for supertype can possibly be @type or @id
        const typeDefine = this.putBody['@type'] ? this.putBody['@type'] : this.putBody['@id'];
        const superType = this.getComponentByRdfType(typeDefine);
        const superFieldOptions = {
          context: this.putBody,
          url: this.scrudResourceURL,
          requestType: this.uiType
        };
        const superTypeNode = new UISchemaNode(superType, superFieldOptions);
        const children = [];
        let afterType = false;
        let previousType;
        Object.keys(this.putBody).forEach(key => {
          // If the keys are after the super type definition
          if (afterType) {
            // Key for rdf type after the supertype can be notated by @id or be the value
            const subTypeDefine = this.putBody[key]['@id'] ? this.putBody[key]['@id'] : this.putBody[key];
            const component = this.getComponentByRdfType(subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine);
            this.getLabelFromKey(key);
            const props = {
              label: key,
              required: false,
              formData: this.putData.details.content[key]
            };
            const node = {
              nodeRdfType: component,
              scrudResourceURL: this.scrudResourceURL,
              configMapping: this.configMapping,
              uiType: this.uiType,
              data: {
                label: null,
                as: component,
                props: props
              },
              optionsBody: this.body,
              putBodyResponse: this.putBody,
              putDataResponse: this.putData
            };
            previousType = subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine;

            if (component !== 'String') {
              children.push(node);
            }
          } else {
            afterType = key === '@id' || key === '@type';
          }
        });
        superTypeNode.setChildren(children);
        this.uiSchemaObject.addNode(superTypeNode);
        this.uiSchema = this.uiSchemaObject.getUISchema();
      }
    },

    getComponentByRdfType(type) {
      const match = this.configMapping.components[type];

      if (match) {
        switch (this.uiType) {
          case 'post':
            return match.input;

          case 'put':
            return match.input;

          case 'get':
            return match.render;
        }
      }

      return 'String';
    },

    getLabelFromKey(key) {
      // Convert key to input label by replacing "_,-,@" with " " and convert to title cases
      let label = key.replace(/[^a-zA-Z0-9]/g, " ");
      return this.toSentenceCase(label);
    },

    toSentenceCase(str) {
      if (str === null || str === '') return false;else str = str.toString();
      return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    }

  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}

/* script */
const __vue_script__ = script;
/* template */

var __vue_render__ = function () {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('SSRFormGen', {
    attrs: {
      "uiSchema": _vm.uiSchema,
      "data": {}
    }
  });
};

var __vue_staticRenderFns__ = [];
/* style */

const __vue_inject_styles__ = undefined;
/* scoped */

const __vue_scope_id__ = undefined;
/* module identifier */

const __vue_module_identifier__ = undefined;
/* functional template */

const __vue_is_functional_template__ = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

const __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, undefined, undefined, undefined);

/* eslint-disable import/prefer-default-export */

var components = /*#__PURE__*/Object.freeze({
  __proto__: null,
  ScrudComponent: __vue_component__
});

// Import vue components

const install = function installScrudComponent(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.entries(components).forEach(([componentName, component]) => {
    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()


const plugin = {
  install
}; // To auto-install on non-es builds, when vue is found

export default plugin;
export { __vue_component__ as ScrudComponent };
