'use strict';Object.defineProperty(exports,'__esModule',{value:true});var _slicedToArray=require('@babel/runtime/helpers/slicedToArray'),_defineProperty=require('@babel/runtime/helpers/defineProperty'),_asyncToGenerator=require('@babel/runtime/helpers/asyncToGenerator'),_regeneratorRuntime=require('@babel/runtime/regenerator'),ssrFormGen=require('@openteamsinc/ssr-form-gen'),_classCallCheck=require('@babel/runtime/helpers/classCallCheck'),_createClass=require('@babel/runtime/helpers/createClass');function _interopDefaultLegacy(e){return e&&typeof e==='object'&&'default'in e?e:{'default':e}}var _slicedToArray__default=/*#__PURE__*/_interopDefaultLegacy(_slicedToArray);var _defineProperty__default=/*#__PURE__*/_interopDefaultLegacy(_defineProperty);var _asyncToGenerator__default=/*#__PURE__*/_interopDefaultLegacy(_asyncToGenerator);var _regeneratorRuntime__default=/*#__PURE__*/_interopDefaultLegacy(_regeneratorRuntime);var _classCallCheck__default=/*#__PURE__*/_interopDefaultLegacy(_classCallCheck);var _createClass__default=/*#__PURE__*/_interopDefaultLegacy(_createClass);var UISchema = /*#__PURE__*/function () {
  function UISchema() {
    _classCallCheck__default['default'](this, UISchema);

    this.uiSchema = {};
  }

  _createClass__default['default'](UISchema, [{
    key: "createNode",
    value: function createNode(as) {
      var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
        required: false,
        children: []
      };
      this.uiSchema[this.generateNextKey()] = new UISchemaNode(as, props);
    }
  }, {
    key: "addNode",
    value: function addNode(node) {
      this.uiSchema[this.generateNextKey()] = node;
    }
  }, {
    key: "getUISchema",
    value: function getUISchema() {
      var _this = this;

      var schema = {};
      Object.keys(this.uiSchema).forEach(function (node, index) {
        schema[index] = _this.uiSchema[node].node;
      });
      return schema;
    }
  }, {
    key: "getNodes",
    value: function getNodes() {
      return this.uiSchema;
    }
  }, {
    key: "generateNextKey",
    value: function generateNextKey() {
      return Object.keys(this.uiSchema).length;
    }
  }]);

  return UISchema;
}();

var UISchemaNode = /*#__PURE__*/function () {
  function UISchemaNode(as) {
    var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
      required: false,
      children: []
    };
    var label = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    _classCallCheck__default['default'](this, UISchemaNode);

    this.node = {
      as: as,
      label: label,
      props: props
    };
  }

  _createClass__default['default'](UISchemaNode, [{
    key: "setChildren",
    value: function setChildren(children) {
      this.node.props.children = children;
    }
  }, {
    key: "setFieldOptions",
    value: function setFieldOptions(props) {
      this.node.props = props;
    }
  }, {
    key: "getSchema",
    value: function getSchema() {
      var schema = {
        as: this.node.as,
        props: this.node.props
      };
      this.node.children.forEach(function (child) {
        schema.props.children.push(child);
      });
      return schema;
    }
  }, {
    key: "getNode",
    value: function getNode() {
      return this.node;
    }
  }, {
    key: "getChildren",
    value: function getChildren() {
      return this.node.props.children;
    }
  }, {
    key: "getFieldOptions",
    value: function getFieldOptions() {
      return this.node.props;
    }
  }, {
    key: "getComponent",
    value: function getComponent() {
      return this.node.as;
    }
  }]);

  return UISchemaNode;
}();function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty__default['default'](target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
var script = {
  props: {
    scrudResourceURL: {
      type: String
    },
    configMapping: {
      type: Object
    },
    uiType: {
      type: String
    },
    nodeRdfType: {
      type: String,
      required: false
    },
    data: {
      type: [Object, Array, String, Number],
      required: false
    },
    componentData: {
      type: Object,
      required: false
    },
    optionsBody: {
      type: Object,
      required: false
    },
    postBodyContext: {
      type: Object,
      required: false
    },
    getBodyResponse: {
      type: Object,
      required: false
    },
    getDataResponse: {
      type: Object,
      required: false
    },
    putBodyResponse: {
      type: Object,
      required: false
    },
    putDataResponse: {
      type: Object,
      required: false
    }
  },
  components: {
    'SSRFormGen': ssrFormGen.SSRFormGen
  },
  data: function data() {
    return {
      uiSchemaObject: new UISchema(),
      uiSchema: {},
      components: [],
      schemaURL: null,
      contextURL: null,
      body: {},
      postBody: {},
      getBody: {},
      putBody: {},
      getData: {},
      putData: {}
    };
  },
  fetch: function fetch() {
    var _this = this;

    return _asyncToGenerator__default['default']( /*#__PURE__*/_regeneratorRuntime__default['default'].mark(function _callee() {
      var requestType;
      return _regeneratorRuntime__default['default'].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!_this.optionsBody) {
                _context.next = 4;
                break;
              }

              _context.t0 = _this.optionsBody;
              _context.next = 7;
              break;

            case 4:
              _context.next = 6;
              return _this.$cachingClient.options(_this.convertToHttps(_this.scrudResourceURL), {
                json: true
              });

            case 6:
              _context.t0 = _context.sent;

            case 7:
              _this.body = _context.t0;
              requestType = _this.body[_this.uiType];
              _context.t1 = _this.uiType;
              _context.next = _context.t1 === 'post' ? 12 : _context.t1 === 'get' ? 23 : _context.t1 === 'put' ? 42 : 61;
              break;

            case 12:
              _this.schemaURL = _this.getPostSchemaURL(requestType);
              _this.contextURL = _this.getPostContextURL(requestType);

              if (!_this.postBodyContext) {
                _context.next = 18;
                break;
              }

              _context.t2 = _this.postBodyContext;
              _context.next = 21;
              break;

            case 18:
              _context.next = 20;
              return _this.$cachingClient.get(_this.convertToHttps(_this.contextURL), {
                json: true
              });

            case 20:
              _context.t2 = _context.sent;

            case 21:
              _this.postBody = _context.t2;
              return _context.abrupt("return", _this.generateUIPost());

            case 23:
              _this.schemaURL = _this.getSchemaURL(requestType);
              _this.contextURL = _this.getContextURL(requestType);

              if (!_this.getBodyResponse) {
                _context.next = 29;
                break;
              }

              _context.t3 = _this.getBodyResponse;
              _context.next = 32;
              break;

            case 29:
              _context.next = 31;
              return _this.$cachingClient.get(_this.convertToHttps(_this.contextURL), {
                json: true
              });

            case 31:
              _context.t3 = _context.sent;

            case 32:
              _this.getBody = _context.t3;

              if (!_this.getDataResponse) {
                _context.next = 37;
                break;
              }

              _context.t4 = _this.getDataResponse;
              _context.next = 40;
              break;

            case 37:
              _context.next = 39;
              return _this.$cachingClient.get(_this.convertToHttps(_this.scrudResourceURL), {
                json: true
              });

            case 39:
              _context.t4 = _context.sent;

            case 40:
              _this.getData = _context.t4;
              return _context.abrupt("return", _this.generateUIGet());

            case 42:
              _this.schemaURL = _this.getPostSchemaURL(requestType);
              _this.contextURL = _this.getPostContextURL(requestType);

              if (!_this.putBodyResponse) {
                _context.next = 48;
                break;
              }

              _context.t5 = _this.putBodyResponse;
              _context.next = 51;
              break;

            case 48:
              _context.next = 50;
              return _this.$cachingClient.get(_this.convertToHttps(_this.contextURL), {
                json: true
              });

            case 50:
              _context.t5 = _context.sent;

            case 51:
              _this.putBody = _context.t5;

              if (!_this.putDataResponse) {
                _context.next = 56;
                break;
              }

              _context.t6 = _this.putDataResponse;
              _context.next = 59;
              break;

            case 56:
              _context.next = 58;
              return _this.$cachingClient.get(_this.convertToHttps(_this.scrudResourceURL), {
                json: true
              });

            case 58:
              _context.t6 = _context.sent;

            case 59:
              _this.putData = _context.t6;
              return _context.abrupt("return", _this.generateUIPut());

            case 61:
              return _context.abrupt("break", 62);

            case 62:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  methods: {
    getPostSchemaURL: function getPostSchemaURL(data) {
      return data.requestBody.content['application/json'].schema;
    },
    getPostContextURL: function getPostContextURL(data) {
      return data.requestBody.content['application/json'].context;
    },
    getSchemaURL: function getSchemaURL(data) {
      return data.responses['200'].content['application/json'].schema;
    },
    getContextURL: function getContextURL(data) {
      return data.responses['200'].content['application/json'].context;
    },
    convertToHttps: function convertToHttps(url) {
      if (url) {
        var to = new URL(url);
        to.protocol = 'https';
        return to.toString();
      }
    },
    generateUIPost: function generateUIPost() {
      var _this2 = this;

      if (this.nodeRdfType) {
        var subType = this.nodeRdfType;
        var children = [];
        var fieldOptions = this.data;
        var superTypeNode = new UISchemaNode(subType, fieldOptions.props, fieldOptions.label);
        superTypeNode.setChildren(children);
        this.uiSchema = superTypeNode;
      } else {
        // Key for supertype can possibly be @type or @id
        var typeDefine = this.postBody['@type'] ? this.postBody['@type'] : this.postBody['@id'];
        var superType = this.getComponentByRdfType(typeDefine);
        var superFieldOptions = {
          context: this.postBody,
          url: this.scrudResourceURL,
          requestType: this.uiType
        };

        var _superTypeNode = new UISchemaNode(superType, superFieldOptions);

        var _children = [];
        var afterType = false;
        var previousType;
        Object.keys(this.postBody).forEach(function (key) {
          // If the keys are after the super type definition
          if (afterType) {
            // Key for rdf type after the supertype can be notated by @id or be the value
            var subTypeDefine = _this2.postBody[key]['@id'] ? _this2.postBody[key]['@id'] : _this2.postBody[key];

            var component = _this2.getComponentByRdfType(subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine);

            _this2.getLabelFromKey(key);

            var props = {
              required: false
            };
            var node = {
              nodeRdfType: component,
              scrudResourceURL: _this2.scrudResourceURL,
              configMapping: _this2.configMapping,
              uiType: _this2.uiType,
              data: {
                label: null,
                as: component,
                props: props
              },
              optionsBody: _this2.body,
              postBodyContext: _this2.postBody
            };
            previousType = subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine;

            if (component !== 'String') {
              _children.push(node);
            }
          } else {
            afterType = key === '@id' || key === '@type';
          }
        });

        _superTypeNode.setChildren(_children);

        this.uiSchemaObject.addNode(_superTypeNode);
        this.uiSchema = this.uiSchemaObject.getUISchema();
      }
    },
    generateUIGet: function generateUIGet() {
      var _this3 = this;

      // Check if component is a leaf node child
      if (this.nodeRdfType) {
        var subType = this.nodeRdfType;

        var fieldOptions = _objectSpread(_objectSpread({}, this.componentData), {}, {
          required: false,
          data: this.data
        });

        var superTypeNode = new UISchemaNode(subType, fieldOptions);
        this.uiSchema = superTypeNode;
      } else {
        if (this.getBody.content && this.getBody.content['@container']) {
          var superType = this.getComponentByRdfType(this.getBody.content['@container']);

          var _superTypeNode2 = new UISchemaNode(superType);

          var _subType = this.getComponentByRdfType(this.getBody.content['openteams:envelopeContents']);

          var href = this.getData.content[0] ? this.getData.content[0].href : false;

          if (href) {
            var children = [];
            this.getData.content.forEach(function (dataItem) {
              var cardFieldOptions = {
                nodeRdfType: _subType,
                data: dataItem,
                scrudResourceURL: _this3.scrudResourceURL,
                configMapping: _this3.configMapping,
                uiType: _this3.uiType,
                optionsBody: _this3.body,
                getBodyResponse: _this3.getBody,
                getDataResponse: _this3.getData
              };
              children.push(cardFieldOptions);
            });

            _superTypeNode2.setChildren(children);
          }

          this.uiSchemaObject.addNode(_superTypeNode2);
          this.uiSchema = this.uiSchemaObject.getUISchema();
        } else {
          var _superType = this.getComponentByRdfType(this.getBody['@id']);

          var superTypeFieldOptions = {
            data: this.getData
          };

          var _superTypeNode3 = new UISchemaNode(_superType, superTypeFieldOptions);

          var _children2 = [];
          var afterType = false;
          var previousType;
          Object.keys(this.getBody).forEach(function (key) {
            if (afterType) {
              var typeDefine = _this3.getBody[key]['@id'] ? _this3.getBody[key]['@id'] : _this3.getBody[key];

              var component = _this3.getComponentByRdfType(typeDefine === 'schema:sameAs' ? previousType : typeDefine);

              var label = _this3.getLabelFromKey(key);

              var _fieldOptions = {
                data: _this3.getData.details.content[key],
                componentData: {
                  label: label
                },
                nodeRdfType: component,
                scrudResourceURL: _this3.scrudResourceURL,
                configMapping: _this3.configMapping,
                uiType: _this3.uiType,
                optionsBody: _this3.body,
                getBodyResponse: _this3.getBody,
                getDataResponse: _this3.getData
              };

              _children2.push(_fieldOptions);

              previousType = typeDefine === 'schema:sameAs' ? previousType : typeDefine;
            } else {
              afterType = key === '@id';
            }
          });

          _superTypeNode3.setChildren(_children2);

          this.uiSchemaObject.addNode(_superTypeNode3);
          this.uiSchema = this.uiSchemaObject.getUISchema();
        }
      }
    },
    generateUIPut: function generateUIPut() {
      var _this4 = this;

      if (this.nodeRdfType) {
        var subType = this.nodeRdfType;
        var children = [];
        var fieldOptions = this.data;
        var superTypeNode = new UISchemaNode(subType, fieldOptions.props, fieldOptions.label);
        superTypeNode.setChildren(children);
        this.uiSchema = superTypeNode;
      } else {
        // Key for supertype can possibly be @type or @id
        var typeDefine = this.putBody['@type'] ? this.putBody['@type'] : this.putBody['@id'];
        var superType = this.getComponentByRdfType(typeDefine);
        var superFieldOptions = {
          context: this.putBody,
          url: this.scrudResourceURL,
          requestType: this.uiType
        };

        var _superTypeNode4 = new UISchemaNode(superType, superFieldOptions);

        var _children3 = [];
        var afterType = false;
        var previousType;
        Object.keys(this.putBody).forEach(function (key) {
          // If the keys are after the super type definition
          if (afterType) {
            // Key for rdf type after the supertype can be notated by @id or be the value
            var subTypeDefine = _this4.putBody[key]['@id'] ? _this4.putBody[key]['@id'] : _this4.putBody[key];

            var component = _this4.getComponentByRdfType(subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine);

            _this4.getLabelFromKey(key);

            var props = {
              label: key,
              required: false,
              formData: _this4.putData.details.content[key]
            };
            var node = {
              nodeRdfType: component,
              scrudResourceURL: _this4.scrudResourceURL,
              configMapping: _this4.configMapping,
              uiType: _this4.uiType,
              data: {
                label: null,
                as: component,
                props: props
              },
              optionsBody: _this4.body,
              putBodyResponse: _this4.putBody,
              putDataResponse: _this4.putData
            };
            previousType = subTypeDefine === 'schema:sameAs' ? previousType : subTypeDefine;

            if (component !== 'String') {
              _children3.push(node);
            }
          } else {
            afterType = key === '@id' || key === '@type';
          }
        });

        _superTypeNode4.setChildren(_children3);

        this.uiSchemaObject.addNode(_superTypeNode4);
        this.uiSchema = this.uiSchemaObject.getUISchema();
      }
    },
    getComponentByRdfType: function getComponentByRdfType(type) {
      var match = this.configMapping.components[type];

      if (match) {
        switch (this.uiType) {
          case 'post':
            return match.input;

          case 'put':
            return match.input;

          case 'get':
            return match.render;
        }
      }

      return 'String';
    },
    getLabelFromKey: function getLabelFromKey(key) {
      // Convert key to input label by replacing "_,-,@" with " " and convert to title cases
      var label = key.replace(/[^a-zA-Z0-9]/g, " ");
      return this.toSentenceCase(label);
    },
    toSentenceCase: function toSentenceCase(str) {
      if (str === null || str === '') return false;else str = str.toString();
      return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    }
  }
};function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
        createInjectorSSR = createInjector;
        createInjector = shadowMode;
        shadowMode = false;
    }
    // Vue.extend constructor export interop.
    const options = typeof script === 'function' ? script.options : script;
    // render functions
    if (template && template.render) {
        options.render = template.render;
        options.staticRenderFns = template.staticRenderFns;
        options._compiled = true;
        // functional template
        if (isFunctionalTemplate) {
            options.functional = true;
        }
    }
    // scopedId
    if (scopeId) {
        options._scopeId = scopeId;
    }
    let hook;
    if (moduleIdentifier) {
        // server build
        hook = function (context) {
            // 2.3 injection
            context =
                context || // cached call
                    (this.$vnode && this.$vnode.ssrContext) || // stateful
                    (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
            // 2.2 with runInNewContext: true
            if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                context = __VUE_SSR_CONTEXT__;
            }
            // inject component styles
            if (style) {
                style.call(this, createInjectorSSR(context));
            }
            // register component module identifier for async chunk inference
            if (context && context._registeredComponents) {
                context._registeredComponents.add(moduleIdentifier);
            }
        };
        // used by ssr in case component is cached and beforeCreate
        // never gets called
        options._ssrRegister = hook;
    }
    else if (style) {
        hook = shadowMode
            ? function (context) {
                style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
            }
            : function (context) {
                style.call(this, createInjector(context));
            };
    }
    if (hook) {
        if (options.functional) {
            // register for functional component in vue file
            const originalRender = options.render;
            options.render = function renderWithStyleInjection(h, context) {
                hook.call(context);
                return originalRender(h, context);
            };
        }
        else {
            // inject component registration as beforeCreate hook
            const existing = options.beforeCreate;
            options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
        }
    }
    return script;
}/* script */
var __vue_script__ = script;
/* template */

var __vue_render__ = function __vue_render__() {
  var _vm = this;

  var _h = _vm.$createElement;

  var _c = _vm._self._c || _h;

  return _c('SSRFormGen', {
    attrs: {
      "uiSchema": _vm.uiSchema,
      "data": {}
    }
  });
};

var __vue_staticRenderFns__ = [];
/* style */

var __vue_inject_styles__ = undefined;
/* scoped */

var __vue_scope_id__ = undefined;
/* module identifier */

var __vue_module_identifier__ = "data-v-1e4ec35a";
/* functional template */

var __vue_is_functional_template__ = false;
/* style inject */

/* style inject SSR */

/* style inject shadow dom */

var __vue_component__ = /*#__PURE__*/normalizeComponent({
  render: __vue_render__,
  staticRenderFns: __vue_staticRenderFns__
}, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, false, undefined, undefined, undefined);/* eslint-disable import/prefer-default-export */var components=/*#__PURE__*/Object.freeze({__proto__:null,ScrudComponent: __vue_component__});var install = function installScrudComponent(Vue) {
  if (install.installed) return;
  install.installed = true;
  Object.entries(components).forEach(function (_ref) {
    var _ref2 = _slicedToArray__default['default'](_ref, 2),
        componentName = _ref2[0],
        component = _ref2[1];

    Vue.component(componentName, component);
  });
}; // Create module definition for Vue.use()


var plugin = {
  install: install
}; // To auto-install on non-es builds, when vue is found
// eslint-disable-next-line no-redeclare

/* global window, global */

{
  var GlobalVue = null;

  if (typeof window !== 'undefined') {
    GlobalVue = window.Vue;
  } else if (typeof global !== 'undefined') {
    GlobalVue = global.Vue;
  }

  if (GlobalVue) {
    GlobalVue.use(plugin);
  }
} // Default export is library as a whole, registered via Vue.use()
exports.ScrudComponent=__vue_component__;exports.default=plugin;